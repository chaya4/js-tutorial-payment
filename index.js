import { IndividualInformation } from "./models/individualInformation.js";
import { WorkingRecord } from "./models/workingRecord.js"
import { MonthlyPayment  } from "./models/monthlyPayment.js"

const IndividualInformationDATA = {};

IndividualInformationDATA.add = (email,individualInformation)=>{
    IndividualInformationDATA[email] = individualInformation;
};


const glock = new IndividualInformation("Boonyarid", "Kluebpoung", "Glock","nighttime.google_@hotmail.com","1234567890", "0931329601", "100")
const ice = new IndividualInformation("Panudet","Khemngern","Ice","Panudet@odds.team","1234567890","0910288798","100");
const kai = new IndividualInformation("The","jitti", "kai", "thekai@odds.com", "123456", "0123456789", "999");


IndividualInformationDATA[glock['Email']] = glock;
IndividualInformationDATA[ice['Email']] = ice;

IndividualInformationDATA.add(kai['Email'],kai)

console.log(IndividualInformationDATA);

console.log("*************************************************");

console.log(IndividualInformationDATA['Panudet@odds.team']);