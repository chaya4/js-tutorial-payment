/*
Monthly Payment 
Name-Surname
Nickname
Email
Account Number
Telephone Number
Working Days
Transfer Amount
Submitted Date
Remark
*/
function MonthlyPayment (
  _Name,
  _Surname,
  _Nickname,
  _Email,
  _Account_Number,
  _Telephone_Number,
  _Working_Days,
  _Transfer_Amount,
  _Submitted_Date,
  _Remark
) {
  this.Name = _Name;
  this.Surname = _Surname;
  this.Nickname = _Nickname;
  this.Email = _Email;
  this.Account_Number = _Account_Number;
  this.Telephone_Number = _Telephone_Number;
  this.Working_Days = _Working_Days;
  this.Transfer_Amount = _Transfer_Amount;
  this.Submitted_Date = _Submitted_Date;
  this.Remark = _Remark;
};

MonthlyPayment.CreateMonthlyPayment = (
  _IndividualInformation,
  _WorkingRecord
) => {
  if (_IndividualInformation && _WorkingRecord) {
    return MonthlyPayment(
      _IndividualInformation["Name"],
      _IndividualInformation["Surname"],
      _IndividualInformation["Nickname"],
      _IndividualInformation["Email"],
      _IndividualInformation["Account_Number"],
      _IndividualInformation["Telephone_Number"],
      _IndividualInformation["Daily_Rate"] * _WorkingRecord["Working_Days"],
      _WorkingRecord["Working_Days"],
      _WorkingRecord["Submitted_Date"],
      _WorkingRecord["Remark"]
    );
  } else {
    return undefined;
  }
};
export { MonthlyPayment };
