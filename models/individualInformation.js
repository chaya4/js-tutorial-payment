/*

Individual Information
Name-Surname
Nickname
Email
Account Number
Telephone Number
Daily Rate

*/

function IndividualInformation (
  _Name,
  _Surname,
  _Nickname,
  _Email,
  _Account_Number,
  _Telephone_Number,
  _Daily_Rate
) {


  this.Name = _Name;
  this.Surname = _Surname;
  this.Nickname = _Nickname;
  this.Email = _Email;
  this.Account_Number = _Account_Number;
  this.Telephone_Number = _Telephone_Number;
  this.Daily_Rate = _Daily_Rate;


};

export {
  IndividualInformation
}
