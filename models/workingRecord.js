/*
Working Record
Email
Working Days
Submitted Date
Site
Remark
*/

function WorkingRecord(_Email, _Working_Days, _Submitted_Date, _Site, _Remark){
  this.Email = _Email;
  this.Working_Days = _Working_Days;
  this.Submitted_Date = _Submitted_Date;
  this.Site = _Site;
  this.Remark = _Remark;
};

export{
  WorkingRecord
};
